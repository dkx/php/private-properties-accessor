<?php

declare(strict_types=1);

namespace DKX\PrivatePropertiesAccessor;

final class PrivatePropertiesAccessor
{


	/** @var object */
	private $object;


	public function __construct(object $object)
	{
		$this->object = $object;
	}


	/**
	 * @param string $property
	 * @return mixed
	 */
	public function & getProperty(string $property)
	{
		$value = & \Closure::bind(function & () use ($property) {
			return $this->$property;
		}, $this->object, $this->object)->__invoke();

		return $value;
	}


	public function getProperties(): array
	{
		return \Closure::bind(function () {
			return \get_object_vars($this);
		}, $this->object, $this->object)->__invoke();
	}

}
