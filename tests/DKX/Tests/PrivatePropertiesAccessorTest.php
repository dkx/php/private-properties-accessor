<?php

declare(strict_types=1);

namespace DKX\PrivatePropertiesAccessorTests;

use DKX\PrivatePropertiesAccessor\PrivatePropertiesAccessor;
use PHPUnit\Framework\TestCase;

final class PrivatePropertiesAccessorTest extends TestCase
{


	public function testGetProperty(): void
	{
		$class = new class
		{
			private $prop;
			public function getProp()
			{
				return $this->prop;
			}
		};

		self::assertNull($class->getProp());

		$accessor = new PrivatePropertiesAccessor($class);
		$prop = & $accessor->getProperty('prop');
		$prop = false;

		self::assertFalse($class->getProp());

		$prop = true;

		self::assertTrue($class->getProp());
	}


	public function testGetProperties(): void
	{
		$class = new class
		{
			private $a;
			private $b;
		};

		$accessor = new PrivatePropertiesAccessor($class);

		self::assertEquals([
			'a' => null,
			'b' => null,
		], $accessor->getProperties());
	}

}
