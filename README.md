# DKX/PrivatePropertiesAccessor

Accessor for private properties without reflection

Based on [https://ocramius.github.io/blog/accessing-private-php-class-members-without-reflection/](https://ocramius.github.io/blog/accessing-private-php-class-members-without-reflection/)

## Installation

```bash
$ composer require dkx/private-properties-accessor
```

## Usage

```php
<?php

declare(strict_types=1);

use DKX\PrivatePropertiesAccessor\PrivatePropertiesAccessor;

$accessor = new PrivatePropertiesAccessor($class);
$prop = & $accessor->getProperty('propFromClass');
$prop = 'hello world';

var_dump($class->propFromClass);    // output: hello world
```

## Get all properties

```php
<?php

$props = $accessor->getProperties();
```

This method uses `get_object_vars` function which means that it will not return parent private properties.
